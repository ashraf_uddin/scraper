<?php
namespace Scraper\Bundle\ScraperBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\DomCrawler\Crawler;
use Scraper\Bundle\ScraperBundle\Document\Product;

class ScraperCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('scrape')
            ->setDescription('Scrape Desired Websites')
            ->addArgument( 'type', InputArgument::OPTIONAL, 'what type of scrape you want to do now?' )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $xml = simplexml_load_file( substr(__DIR__, 0, -7) . 'Resources\config\config.xml');
		$data = json_decode(json_encode((array)$xml),1);
		$parameters = $data['parameters'];
		
		$type = $input->getArgument('type');
        if( $type == "api" ) {
			// auth_conneteion
		}
        
		$crawler = new Crawler($data['parameters']['parameter']);
		$product = new Product();
		
		$i = 0;
		foreach( $data['services']['service'] as $row ) {
			$rows_data = $crawler->filterXPath($row);
			$product->setDataHtml( $rows_data );
			$dm = $this->get('doctrine_mongodb')->getManager();
			$dm->persist($product);
			$dm->flush();
			if( $product->getId() ) $i++;
		}
		
		if( $i > 0 ) $responce = "Work done! Data saving success. Total $i data rows Insert ";
		else $responce = "Error on savaing data!";
		
		$output->writeln( "Responce:- " . $responce );
    }
}