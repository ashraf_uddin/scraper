<?php

namespace Scraper\Bundle\ScraperBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

class Product
{
    /**
     * @MongoDB\Id
     */
    public $id;

    /**
     * @MongoDB\Float
     */
    public $data_html;
	

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataHtml
     *
     * @param string $dataHtml
     * @return self
     */
    public function setDataHtml($dataHtml)
    {
        $this->data_html = $dataHtml;
        return $this;
    }

    /**
     * Get dataHtml
     *
     * @return string $dataHtml
     */
    public function getDataHtml()
    {
        return $this->data_html;
    }
}
