<?php

namespace Scraper\Bundle\ScraperBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Scraper\Bundle\ScraperBundle\Document\Product;

class DefaultController extends Controller
{
    public function indexAction()
    {
		$id = "552b866eda50da501c000032";
		$product = $this->get('doctrine_mongodb')
					->getRepository('ScraperBundle:Product')
					->find( $id );
		echo $product->data_html; exit;
    }
}
