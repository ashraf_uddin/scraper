<?php

namespace Scraper\Bundle\ScraperBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Scraper\Bundle\ScraperBundle\Document\Product;

class UrlController extends Controller
{
    public function indexAction()
    {
        $url = "http://ashraf/test/sample.php";
		$strat_tags = '<table class="displaytable ext-grid full-table">';
		$end_tags = '</table>';
		
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data_html = curl_exec ($ch);
		
		curl_close ($ch);
		
		$explode = explode( $strat_tags, $data_html );
		$return = explode( $end_tags, $explode[1] );
		$server_output = $strat_tags . " " . $return[0] . " " . $end_tags;
		
		//$server_output = file_get_contents($url);
		//echo "<pre>";print_r( $server_output ); exit;
		
		$product = new Product();
		
		$product->setDataHtml( $server_output );
	
		$dm = $this->get('doctrine_mongodb')->getManager();
		$dm->persist($product);
		$dm->flush();
	
		$get_id = $product->getId();
		
		if( $get_id ) $responce = "Work done! Data saving success. ID - $get_id";
		else $responce = "Error on savaing data!";
		
		return $this->render('ScraperBundle:Default:api.html.twig', array( 'responce' => $responce ));
    }
}
