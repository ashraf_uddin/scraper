<?php

namespace Scraper\Bundle\ScraperBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Scraper\Bundle\ScraperBundle\Document\Product;

class ApiController extends Controller
{
    public function indexAction()
    {
        $get = file_get_contents('bundles/sample.xml');
		$arr = simplexml_load_string($get);
		$url = "$arr->url?app_key=$arr->app_key&app_key_val=$arr->app_key_val";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);
		curl_close ($ch);
		
		$product = new Product();
		$product->setDataHtml( $server_output );
	
		$dm = $this->get('doctrine_mongodb')->getManager();
		$dm->persist($product);
		$dm->flush();
	
		$get_id = $product->getId();
		if( $get_id ) $responce = "Work done! Data saving success. ID - $get_id";
		else $responce = "Error on savaing data!";
		return $this->render('ScraperBundle:Default:api.html.twig', array('responce' => $responce));
    }
}
